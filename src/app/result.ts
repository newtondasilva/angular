export interface Result {
    plan: string,
    value: number,
    time: number,
}
