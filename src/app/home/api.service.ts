import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { map, catchError } from 'rxjs/operators';

@Injectable()
export class ApiService {

  constructor(private httpClient: HttpClient) { }

  getFees(): Observable<any> {
    return this.httpClient
    .cache()
    .get('ddd/details');
  }

  getPlans(): Observable<any> {
    return this.httpClient
    .cache()
    .get('plans');
  }

  getPrincing(): Observable<any> {
    return this.httpClient
    .cache()
    .get('ddd/pricing');
  }

}
