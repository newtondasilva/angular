import { TestBed, inject, async } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { CoreModule, HttpCacheService } from '@app/core';
import { ApiService } from './api.service';

describe('ApiService', () => {
  let apiService: ApiService;
  let httpMock: HttpTestingController;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        CoreModule,
        HttpClientTestingModule
      ],
      providers: [
        HttpCacheService,
        ApiService
      ]
    });
  }));

  beforeEach(inject([
    HttpCacheService,
    ApiService,
    HttpTestingController
  ], (htttpCacheService: HttpCacheService,
      _apiService: ApiService,
      _httpMock: HttpTestingController) => {

    apiService = _apiService;
    httpMock = _httpMock;

    htttpCacheService.cleanCache();
  }));

  afterEach(() => {
    httpMock.verify();
  });

  describe('getPricing', () => {
    
  });
});
