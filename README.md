# Project CI&T Telecom

CI&T challenge web project from Newton da Silva

Starter Kit from [https://github.com/ngx-rocket/starter-kit]


# Getting started

1. Go to project folder and install dependencies:
 ```bash
 npm install
 ```
 
2. Launch development server, and open `localhost:4200` in your browser:
 ```bash
 npm start
 ```
 
# Project structure

```
dist/                        compiled version
src/                         project source code
|- app/                      app components
|  |- core/                  core module
|  |- home/                  project page
|    |- api.service.ts       API Service
|  |- result.ts              Result interface used in home
|  +- ...                    additional modules and components
|- assets/                   app assets (images)
|- environments/             values for various build environments
|- theme/                    app global scss variables and theme
|- translations/             translations files
|- index.html                html entry point
|- main.scss                 global style entry point
|- main.ts                   app entry point
|- polyfills.ts              polyfills needed by Angular
+- test.ts                   unit tests entry point
reports/                     test and coverage reports
proxy.conf.js                backend proxy configuration
```